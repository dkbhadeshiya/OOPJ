/*
 * A program to simply calculate the sum of the elements given in the range.
 * You need to provide the range by Command line arguments.
 * Usage:
 *  java SumRange <lowerrange> <upperrange>
 * Author : Dhruman Bhadeshiya <dhrumangajjar@gmail.com>
*/
class SumRange
{
  public static void main(String[] arg)
  {
    int answer=0;
    System.out.println("You've Entered the Range of "+arg[0]+" - "+arg[1]);
    System.out.println("Now Fetching you the sum of the range...");
    for(int i= Integer.parseInt(arg[0]); i<= Integer.parseInt(arg[1]);i++)
    {
        answer += i;
    }
    System.out.println("Answer is : " + answer);
  }
}
