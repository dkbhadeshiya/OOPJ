# OOPJ
Laboratory tutorials for Object Oriented Programming with Java.

This repository contains various codes developed while the laboratory sessions of the Subject called Object Oriented Programming with Java. May contain some basic programs with much basic concepts based on the GTU syllabus.

* First set the path of jdk/bin in the environment variable.
* Compile the code using any JDK to do that:
 > javac filename.java
* after succesful compilatoin you'll have a ClassName.class file in the working directory
* To execute the .class files you need to type:
  > java ClassName
* Remember not to type ClassName.class while executing
