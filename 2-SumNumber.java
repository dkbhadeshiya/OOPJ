/*
 * Basic Java program to take the arguments from the command line
 * and print the sum of the Numbers.
 * Usage:
 *	java SumNumber num1 num2
 * Using Integer.parseInt() for parsing the recieved string argument into integer.
 * Author: Dhruman Bhadeshiya <dhrumangajjar@gmail.com>
*/
class SumNumber
{
	public static void main(String[] arg)
	{
		System.out.println("\n\nGiven Numbers are : " + arg[0] + " , " + arg[1]);
		int ans;
		ans = Integer.parseInt(arg[0]) + Integer.parseInt(arg[1]);
		System.out.println("\nAnswer is : " + ans);
	}
}
