/*
 * Basic Calculator in Java using else if
 * Accepts arguments by Command Line
 * Usage :
 *	java BCIf choice var1 var2
 * Using Integer.parseInt() to convert command line arguments into the integer.
 * Author: Dhruman Bhadeshiya <dhrumangajjar@gmail.com>
*/
class BCIf
{
	public static void main(String[] arg)
	{
		System.out.println("Your choice is : " + arg[0]);
		int a = Integer.parseInt(arg[0]);
		if(a == 1)
			System.out.printf("Sum is :  %d" ,Integer.parseInt(arg[1])+Integer.parseInt(arg[2]));
		else if(a == 2)
			System.out.printf("Substraction is : %d", Integer.parseInt(arg[1])-Integer.parseInt(arg[2]));
		else if(a == 3)
			System.out.printf("Multiplication is : %d", Integer.parseInt(arg[1])*Integer.parseInt(arg[2]));
		else if(a == 4)
			System.out.printf("Division is : %f", Float.parseFloat(arg[1])/Float.parseFloat(arg[2]));
		else
			System.out.println("Wrnog Choice you've made...");
	}
}
