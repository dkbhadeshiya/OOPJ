/*
 * The first Java program written to print Hello World on the Terminal
 * Usage :
 *	java HelloWorld
 * Author: Dhruman Bhadeshiya <dhrumangajjar@gmail.com>
*/
class HelloWorld
{
	public static void main(String[] arg)
	{
		System.out.println( "Hello World!" );
	}
}
